ตัวอย่างโปรเจค nodejs login แบบ JWT ไม่รวมระบบ register

สั่ง npm install เพื่อ install dependence ใน project
สั่ง npm run dev เพิ่อ รันโปรเจค

โปรเจคมีทั้งหมด 3 หน้า ได้แก่
หน้า login ใช้สำหรับ login
หน้า home ใช้เป็นหน้าแรกของระบบ (จำเป็นต้อง login ก่อนถึงเข้าหน้านี้ได้)
หน้า error 401 ใช้เป็นหน้าแสดง error เมื่อผู้ใช้งาน ยังไม่ได้ล็อกอินเข้าสู่ระบบก่อนใช้งาน

หมายเหตุ
ในโปรเจคนี้จะใช้ mock data user สำหรับ ทดสอบ คือ (user1 password1) ในกรณีนำไปใช้จะต้อง implement api /sigin เพิ่มในส่วนของการนำข้อมูล username password ไปเช็คใน ฐานข้อมูล 


ศึกษาหลายระเอียดเพิ่มเติมที่ https://www.sohamkamani.com/blog/javascript/2019-03-29-node-jwt-authentication/