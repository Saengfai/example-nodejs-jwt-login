const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken')
const path = require('path')

const app = express()
app.use(bodyParser.json())
app.use(cookieParser())

const jwtKey = 'my_secret_key';
const jwtExpirySeconds = 300;

const signIn = (req, res) => {
    const users = {
        user1: 'password1',
        user2: 'password2'
    }
    // Get credentials from JSON body
    const { username, password } = req.body
    if (!username || !password || users[username] !== password) {
        // return 401 error is username or password doesn't exist, or if password does
        // not match the password in our records
        return res.status(401).end()
    }

    // Create a new token with the username in the payload
    // and which expires 300 seconds after issue
    const token = jwt.sign({ username }, jwtKey, {
        algorithm: 'HS256',
        expiresIn: jwtExpirySeconds
    })
    console.log('token:', token)

    // set the cookie as the token string, with a similar max age as the token
    // here, the max age is in milliseconds, so we multiply by 1000
    res.cookie('token', token, { maxAge: jwtExpirySeconds * 1000 })
    res.end();
}

const verifyToken = (req, res, next) => {
    const token = req.cookies.token
    // if the cookie is not set, return an unauthorized error
    if (!token) {
        return res.status(401).sendFile(path.join(__dirname+'/page/error401.html'));
    }
    try {
        // Parse the JWT string and store the result in `payload`.
        // Note that we are passing the key in this method as well. This method will throw an error
        // if the token is invalid (if it has expired according to the expiry time we set on sign in),
        // or if the signature does not match
        const payload = jwt.verify(token, jwtKey)
    } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
        // if the error thrown is because the JWT is unauthorized, return a 401 error
        return res.status(401).sendFile(path.join(__dirname+'/page/error401.html'));
        }
        // otherwise, return a bad request error
        return res.status(400).end()
    }
    next();
}

const refresh = (req, res) => {
    // (BEGIN) The code uptil this point is the same as the first part of the `welcome` route
    const token = req.cookies.token
  
    if (!token) {
      return res.status(401).end()
    }
  
    var payload
    try {
      payload = jwt.verify(token, jwtKey)
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        return res.status(401).end()
      }
      return res.status(400).end()
    }
    // (END) The code uptil this point is the same as the first part of the `welcome` route
  
    // We ensure that a new token is not issued until enough time has elapsed
    // In this case, a new token will only be issued if the old token is within
    // 30 seconds of expiry. Otherwise, return a bad request status
    const nowUnixSeconds = Math.round(Number(new Date()) / 1000)
    if (payload.exp - nowUnixSeconds > 30) {
      return res.status(400).end()
    }
  
    // Now, create a new token for the current user, with a renewed expiration time
    const newToken = jwt.sign({ username: payload.username }, jwtKey, {
      algorithm: 'HS256',
      expiresIn: jwtExpirySeconds
    })
  
    // Set the new token as the users `token` cookie
    res.cookie('token', newToken, { maxAge: jwtExpirySeconds * 1000, httpOnly: true })
    res.end()
}

app.get('/login', (req, res)=>{
    res.sendFile(path.join(__dirname+'/page/login.html'));
});

app.get('/logout', (req, res ) => {
    res.cookie("token", "");
    res.clearCookie("token");
    res.redirect('/login');
});

app.get('/', verifyToken, (req, res)=>{
    res.sendFile(path.join(__dirname+'/page/home.html'));
});

// this api not require authorization
app.post('/api1', (req, res)=>{
    res.json('api 1')
})

// this api require authorization
app.post('/api2', verifyToken, (req, res)=>{
    res.json('api 2')
})

app.get('/login', (req, res)=>{
    res.sendFile(path.join(__dirname+'/page/login.html'));
});


app.post('/signIn', signIn);
app.post('/refresh', refresh);


app.listen(3000)